![ShellGen Logo](https://gitlab.com/thejoker3000/shellgen/-/raw/main/logo.png)

# ShellGen (Shell Generator)

This is a simple script that will generate a specific or all shellcodes for CTFs or any other thing.

## INFORMATION

I am not updating this tool but soon will start again (hopefully).
If you would like to add things, feel free

== Aug 7 2021 @ 7:33 UTC ==

There are some errors that are happening and I'm trying to fix it and improve it.
If you'd like to add something, create a pull request.


## Usage

For help:
- `shellgen -h`
- `shellgen --help`

If you want to skip update and just get the reverse shell, add `--no-update` to your commands and it will skip it.

To update:
- `./shellgen.py -u`
- `./shellgen.py --update`

List shells available:
- `shellgen --shells`
- `shellgen -ls`

An example for using:
- `shellgen --lhost 10.10.12.3 --lport 1234 --shell netcat`

Now, available in **version 1.3** it will let you know when executing if there is a new update or not.
Also, you can pass the name of your interface you would like to use instead of having to give the IP. Yes, you can use both but much easy if you gave the interface name.

Example: `./shellgen --lhost eth0 --lport 4444 -s powershell`


# TODO

- Add option to encode shells if needed (this is maybe)


# Kudos

Thanks to [TrustedSec](https://github.com/trustedsec), some code here has been used.
